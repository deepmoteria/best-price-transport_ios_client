//
//  PickUpVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 26/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.

#import "PickUpVC.h"
#import "MenuCollectionViewCell.h"

@interface PickUpVC ()
{
    NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier,*placeMarkArr,*years,*makes,*models,*trims,*searchedArray;
    NSString *address_type,*strForLatitude,*strForLongitude,*strForSourceLatitude,*strForSourceLongitude,*strForDestinationLatitude,*strForDestinationLongitude,*strForCurLatitude,*strForCurLongitude,*strForPickerValue,*strForMinimunYear,*strForMaximunYear,*pickerType,*strForStateId,*strMakeId,*strForRunning,*strForOpen,*searchTextString;
    CLLocationManager *locationManager;
    NSDictionary* aPlacemark;
    NSMutableDictionary *dictForState;
    BOOL is_select;
}
@end

@implementation PickUpVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [APPDELEGATE showLoadingWithTitle:@""];
    [self getAllMyCards];
    [self.txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    locationManager = [[CLLocationManager alloc] init];
    [self getCurrentLocation:self];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForState=[[NSMutableArray alloc] init];
    placeMarkArr=[[NSMutableArray alloc] init];
    aPlacemark=[[NSDictionary alloc] init];
    years = [[NSMutableArray alloc] init];
    makes = [[NSMutableArray alloc] init];
    models = [[NSMutableArray alloc] init];
    trims = [[NSMutableArray alloc] init];
    searchedArray = [[NSMutableArray alloc] init];
    [self getYears];
    [NSPref SetBoolPreference:PREF_REQUEST_ACCEPT Value:NO];
    self.viewForDatePicker.hidden=YES;
    self.viewForSearch.hidden=YES;
    
    [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateSelected];
    [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    [self getState];
    [self.navigationController setNavigationBarHidden:NO];
    [self SetLocalization];
    //[self onClickBtnYear:nil];
    self.viewForCreateRequest.hidden=YES;
    self.viewForMap.hidden=YES;
    
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_HOME", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_PAYMENT", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_payment",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc] initWithObjects:@"Home",SEGUE_TO_PROFILE,SEGUE_TO_PAYMENT,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"logout", nil];
}
-(void)viewDidAppear:(BOOL)animated
{
    strForLatitude=strForCurLatitude;
    strForLongitude=strForCurLongitude;
    
    strForSourceLatitude=strForLatitude;
    strForSourceLongitude=strForLongitude;
    
    strForDestinationLatitude=strForLatitude;
    strForDestinationLongitude=strForLongitude;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue] zoom:14];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    mapView_.delegate=self;
    [self.mapView addSubview:mapView_];
    [self CheckAppStatus];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateSelected];
    [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
    self.viewForDatePicker.hidden=YES;
}

#pragma mark -
#pragma mark - App Status

-(void)CheckAppStatus
{
    NSString *strReqId=[NSPref GetPreference:PREF_REQUEST_ID];
    if(strReqId!=nil)
    {
        [self checkForRequestStatus];
    }
    else
    {
        [self RequestInProgress];
    }
}
-(void)checkForRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",P_GET_REQUEST,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN],PARAM_REQUEST_ID,[NSPref GetPreference:PREF_REQUEST_ID]];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     if([[response valueForKey:@"wipe_out"]boolValue])
                     {
                         [self RemovePreferences];
                     }
                     else
                     {
                         [NSPref SetPreference:PREF_REQUEST_DESCRIPTION Value:[response valueForKey:@"description"]];
                         [NSPref SetPreference:PREF_SOURCE_ADDRESS Value:[response valueForKey:@"src_address"]];
                         [NSPref SetPreference:PREF_DESTINATION_ADDRESS Value:[response valueForKey:@"dest_address"]];
                         [NSPref SetPreference:PREF_VEHICLE_MAKE Value:[response valueForKey:@"make"]];
                         [NSPref SetPreference:PREF_VEHICLE_MODEL Value:[response valueForKey:@"model"]];
                         [NSPref SetPreference:PREF_VEHICLE_YEAR Value:[response valueForKey:@"year"]];
                         [NSPref SetPreference:PREF_VEHICLE_TRIM Value:[response valueForKey:@"trim"]];
                         [NSPref SetPreference:PREF_EXPIRY_DATE Value:[response valueForKey:@"exp_time"]];
                         if([[response valueForKey:@"status"]boolValue])
                         {
                             [NSPref SetBoolPreference:PREF_REQUEST_ACCEPT Value:YES];
                         }
                         else
                         {
                             [NSPref SetBoolPreference:PREF_REQUEST_ACCEPT Value:NO];
                         }
                         [self performSegueWithIdentifier:SEGUE_TO_BID sender:self];
                     }
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                 }
             }
             else
             {
                 
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)RequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",P_GET_REQUEST_PROGRESS,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN]];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [NSPref SetPreference:PREF_REQUEST_ID Value:[response valueForKey:@"request_id"]];
                     [self CheckAppStatus];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Custom methods

-(NSMutableDictionary *)getJsonDictionary:(NSString *)strData
{
    strData = [[[strData stringByReplacingOccurrencesOfString:@"?" withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSArray *objects = [strData componentsSeparatedByString:@";"];
    
    NSData *data = [[objects objectAtIndex:0] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *jsonError;
    
    NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers
                                                                  error:&jsonError];
    NSLog(@"json = %@",dict);
    
    return dict;
}

-(void)setText:(UILabel *)lbl
{
    lbl.numberOfLines=0;
    lbl.lineBreakMode=NSLineBreakByWordWrapping;
    lbl.font=[FontStyleGuide fontRegular:19.0f];
    lbl.textAlignment = NSTextAlignmentLeft;
}

-(void)getYears
{
    if([APPDELEGATE connected])
    {
        strForMinimunYear = @"1940";
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        strForMaximunYear = [formatter stringFromDate:[NSDate date]];
        
        years = [[NSMutableArray alloc] init];
        for (int i=[strForMinimunYear intValue];i<=[strForMaximunYear intValue]; i++)
        {
            [years addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        [searchedArray removeAllObjects];
        [searchedArray addObjectsFromArray:years];
        
        [self.tableForVehicleDetails reloadData];
        self.viewForSearch.hidden=NO;
        [APPDELEGATE hideLoadingView];
        
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)getMakes
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@""];
        NSString *urlString = [NSString stringWithFormat:@"%@?%@=%@",P_GET_MAKES,P_YEAR,self.btnYear.titleLabel.text];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:urlString withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
             [APPDELEGATE hideLoadingView];
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     makes = [[NSMutableArray alloc] init];
                     makes = [response valueForKey:@"Makes"];
                     [searchedArray removeAllObjects];
                     [searchedArray addObjectsFromArray:makes];
                 }
                 else
                 {
                     [searchedArray removeAllObjects];
                     makes = [[NSMutableArray alloc] init];
                 }
                 
                 [self.tableForVehicleDetails reloadData];
                 self.viewForSearch.hidden=NO;
             }
             
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)getModels
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@""];
        
        /*if([self.btnMake.titleLabel.text containsString:@" "])
        {
            self.btnMake.titleLabel.text = [self.btnMake.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        }*/
        
        NSString *urlString = [NSString stringWithFormat:@"%@?%@=%@&%@=%@",P_GET_MAKES,P_YEAR,self.btnYear.titleLabel.text,P_MAKE,self.btnMake.titleLabel.text];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:urlString withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
                 [APPDELEGATE hideLoadingView];
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     models = [[NSMutableArray alloc] init];
                     models = [response valueForKey:@"Makes"];
             
                     [searchedArray removeAllObjects];
                     [searchedArray addObjectsFromArray:models];
                 }
                 else
                 {
                     [searchedArray removeAllObjects];
                     models = [[NSMutableArray alloc] init];
                 }
                 
                 [self.tableForVehicleDetails reloadData];
                 self.viewForSearch.hidden=NO;
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

-(void)getTrims
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@""];
        
        /*if([self.btnMake.titleLabel.text containsString:@" "])
        {
            self.btnMake.titleLabel.text = [self.btnMake.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        }
        
        if([self.btnModel.titleLabel.text containsString:@" "])
        {
            self.btnModel.titleLabel.text = [self.btnModel.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        }*/
        
        NSString *urlString = [NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",P_GET_MAKES,P_YEAR,self.btnYear.titleLabel.text,P_MAKE,self.btnMake.titleLabel.text,P_MODEL,self.btnModel.titleLabel.text];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:urlString withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
                 [APPDELEGATE hideLoadingView];
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     trims = [[NSMutableArray alloc] init];
                     trims = [response valueForKey:@"Makes"];
             
                     [searchedArray removeAllObjects];
                     [searchedArray addObjectsFromArray:trims];
                 }
                 else
                 {
                     [searchedArray removeAllObjects];
                     trims = [[NSMutableArray alloc] init];
                 }
                 
                 [self.tableForVehicleDetails reloadData];
                 self.viewForSearch.hidden=NO;
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)updateSearchArray
{
    if (searchTextString.length != 0)
    {
        searchedArray = [NSMutableArray array];
        
        if([pickerType isEqualToString:@"state"])
        {
            for (int i=0; i<arrForState.count;i++)
            {
                NSString *str = [[arrForState objectAtIndex:i]valueForKey:@"state_name"];
                if ([[str lowercaseString] rangeOfString:[searchTextString lowercaseString]].location != NSNotFound)
                {
                    NSString *str = [arrForState objectAtIndex:i];
                    [searchedArray addObject:str];
                }
            }
        }
        else if([pickerType isEqualToString:@"year"])
        {
            for (int i=0; i<years.count;i++)
            {
                NSString *str = [years objectAtIndex:i];
                if ([[str lowercaseString] rangeOfString:[searchTextString lowercaseString]].location != NSNotFound)
                {
                    NSString *str = [years objectAtIndex:i];
                    [searchedArray addObject:str];
                }
            }
        }
        else if([pickerType isEqualToString:@"make"])
        {
            for (int i=0; i<makes.count;i++)
            {
                NSString *str = [[makes objectAtIndex:i]valueForKey:@"make_display"];
                if ([[str lowercaseString] rangeOfString:[searchTextString lowercaseString]].location != NSNotFound)
                {
                    NSString *str = [makes objectAtIndex:i];
                    [searchedArray addObject:str];
                }
            }
        }
        else if([pickerType isEqualToString:@"model"])
        {
            for (int i=0; i<models.count;i++)
            {
                NSString *str = [[models objectAtIndex:i] valueForKey:@"model_name"];
                if ([[str lowercaseString] rangeOfString:[searchTextString lowercaseString]].location != NSNotFound)
                {
                    NSString *str = [models objectAtIndex:i];
                    [searchedArray addObject:str];
                }
            }
        }
        else if([pickerType isEqualToString:@"trim"])
        {
            for (int i=0; i<trims.count;i++)
            {
                NSString *str = [[trims objectAtIndex:i]valueForKey:@"model_trim"];
                if ([[str lowercaseString] rangeOfString:[searchTextString lowercaseString]].location != NSNotFound)
                {
                    NSString *str = [trims objectAtIndex:i];
                    [searchedArray addObject:str];
                }
            }
        }
    }
    else
    {
        [searchedArray removeAllObjects];
        
        if([pickerType isEqualToString:@"state"])
            [searchedArray addObjectsFromArray:arrForState];
        
        else if([pickerType isEqualToString:@"year"])
            [searchedArray addObjectsFromArray:years];
        
        else if([pickerType isEqualToString:@"make"])
            [searchedArray addObjectsFromArray:makes];
        
        else if([pickerType isEqualToString:@"model"])
            [searchedArray addObjectsFromArray:models];
        
        else if([pickerType isEqualToString:@"trim"])
            [searchedArray addObjectsFromArray:trims];
    }
    
    [self.tableForVehicleDetails reloadData];
}

-(void)getAlert
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:P_GET_ALERT withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
                 [APPDELEGATE hideLoadingView];
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     NSString *strAlert = [response valueForKey:@"alert_msg"];
                     UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:strAlert delegate:self cancelButtonTitle:NSLocalizedString(@"NO_THANKS", nil) otherButtonTitles:NSLocalizedString(@"AGREE", nil), nil];
                     alert.tag=300;
                     [alert show];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

-(void)createRequest
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"CREATING_REQUEST", nil)];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
    [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
    [dictParam setObject:strForSourceLatitude forKey:PARAM_SOURCE_LATITUDE];
    [dictParam setObject:strForSourceLongitude forKey:PARAM_SOURCE_LONGITUDE];
    [dictParam setObject:strForDestinationLatitude forKey:PARAM_DESTINATION_LATITUDE];
    [dictParam setObject:strForDestinationLongitude forKey:PARAM_DESTINATION_LONGITUDE];
    [dictParam setObject:self.txtPickupAddress.text forKey:PARAM_SOURCE_ADDRESS];
    [dictParam setObject:self.txtDropoffAddress.text forKey:PARAM_DESTINATION_ADDRESS];
    [dictParam setObject:strForStateId forKey:PARAM_STATE_ID];
    [dictParam setObject:strMakeId forKey:PARAM_MAKE];
    [dictParam setObject:self.btnModel.titleLabel.text forKey:PARAM_MODEL];
    [dictParam setObject:self.btnYear.titleLabel.text forKey:PARAM_YEAR];
    [dictParam setObject:self.btnTrim.titleLabel.text forKey:PARAM_TRIM];
    [dictParam setObject:strForRunning forKey:PARAM_OPERATIONAL];
    [dictParam setObject:strForOpen forKey:PARAM_TRANSPORT];
    
    if ([self.textViewForDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
    {
        [dictParam setObject:@"" forKey:PARAM_DESCRIPTION];
    }
    else
    {
        [dictParam setObject:self.textViewForDescription.text forKey:PARAM_DESCRIPTION];
    }
    
    AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:P_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if([[response valueForKey:@"success"] boolValue])
             {
                 NSLog(@"CreateRequest response ------> %@",response);
                 [NSPref SetPreference:PREF_REQUEST_ID Value:[response valueForKey:@"request_id"]];
                 [NSPref SetPreference:PREF_REQUEST_DESCRIPTION Value:[response valueForKey:@"description"]];
                 [NSPref SetPreference:PREF_SOURCE_ADDRESS Value:[response valueForKey:@"src_address"]];
                 [NSPref SetPreference:PREF_DESTINATION_ADDRESS Value:[response valueForKey:@"dest_address"]];
                 [NSPref SetPreference:PREF_VEHICLE_MAKE Value:[response valueForKey:@"make"]];
                 [NSPref SetPreference:PREF_VEHICLE_MODEL Value:[response valueForKey:@"model"]];
                 [NSPref SetPreference:PREF_VEHICLE_YEAR Value:[response valueForKey:@"year"]];
                 [NSPref SetPreference:PREF_VEHICLE_TRIM Value:[response valueForKey:@"trim"]];
                 [NSPref SetPreference:PREF_EXPIRY_DATE Value:[response valueForKey:@"exp_time"]];
                 [NSPref SetBoolPreference:PREF_IS_RUNNING Value:[[response valueForKey:@"is_operational"] boolValue]];
                 [NSPref SetBoolPreference:PREF_IS_TRANSPORT Value:[[response valueForKey:@"type_transport"] boolValue]];
                 [APPDELEGATE showToastMessage:NSLocalizedString(@"CREATE_REQUEST_SUCCESS", nil)];
                 [self performSegueWithIdentifier:SEGUE_TO_BID sender:self];
             }
             else
             {
                 NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                 if([str isEqualToString:@"21"])
                 {
                     [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                 }
                 {
                     [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                 }
             }
         }
     }];
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    [self.ScrollObj setContentSize:CGSizeMake(self.viewForCreateRequest.frame.size.width, 540)];
    
    self.lblWelcome.text=NSLocalizedString(@"WELCOME", nil);
    self.lblwelcome2.text=NSLocalizedString(@"WELCOME_NEXT", nil);
    self.lblAddress.text=NSLocalizedString(@"ADDRESS", nil);
    self.lblVehicleDetail.text=NSLocalizedString(@"VEHICLE_DETAIL", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    self.lblRunning.text=NSLocalizedString(@"RUNNING", nil);
    self.lblNotRunning.text=NSLocalizedString(@"NOT_RUNNING", nil);
    self.lblOpen.text=NSLocalizedString(@"OPEN", nil);
    self.lblEnclosed.text=NSLocalizedString(@"ENCLOSED", nil);
    
    self.textViewForMessage.text=NSLocalizedString(@"WELCOME_MESSAGE", nil);
    self.textViewForDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
    
    self.txtPickupAddress.placeholder=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.txtDropoffAddress.placeholder=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.txtState.placeholder=NSLocalizedString(@"STATE", nil);
    self.txtSearch.placeholder=NSLocalizedString(@"SEARCH", nil);
    
    [self.btnPickUp setTitle:NSLocalizedString(@"PICKUP", nil) forState:UIControlStateNormal];
    [self.btnPickUp setTitle:NSLocalizedString(@"PICKUP", nil) forState:UIControlStateSelected];
    [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateSelected];
    [self.btnPlaceRequest setTitle:NSLocalizedString(@"PLACE_REQUEST", nil) forState:UIControlStateNormal];
    [self.btnPlaceRequest setTitle:NSLocalizedString(@"PLACE_REQUEST", nil) forState:UIControlStateSelected];
    [self.btnDatePickCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateNormal];
    [self.btnDatePickCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateSelected];
    [self.btnDatePickDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateNormal];
    [self.btnDatePickDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateSelected];
    
    [self.btnYear setTitle:NSLocalizedString(@"YEAR", nil) forState:UIControlStateNormal];
    [self.btnYear setTitle:NSLocalizedString(@"YEAR", nil) forState:UIControlStateSelected];
    [self.btnMake setTitle:NSLocalizedString(@"MAKE", nil) forState:UIControlStateNormal];
    [self.btnMake setTitle:NSLocalizedString(@"MAKE", nil) forState:UIControlStateSelected];
    [self.btnModel setTitle:NSLocalizedString(@"MODEL", nil) forState:UIControlStateNormal];
    [self.btnModel setTitle:NSLocalizedString(@"MODEL", nil) forState:UIControlStateSelected];
    [self.btnTrim setTitle:NSLocalizedString(@"TRIM", nil) forState:UIControlStateNormal];
    [self.btnTrim setTitle:NSLocalizedString(@"TRIM", nil) forState:UIControlStateSelected];
    [self.btnSelectState setTitle:NSLocalizedString(@"STATE", nil) forState:UIControlStateNormal];
    [self.btnSelectState setTitle:NSLocalizedString(@"STATE", nil) forState:UIControlStateSelected];
    
    self.btnSelectState.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    [self.btnMake setUserInteractionEnabled:NO];
    [self.btnModel setUserInteractionEnabled:NO];
    [self.btnTrim setUserInteractionEnabled:NO];
    
    
    [self.btnYear setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnYear setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.btnModel setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnModel setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.btnMake setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnMake setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.btnTrim setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnTrim setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.btnSelectState setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnSelectState setTitleColor:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    
    [self.txtPickupAddress setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtDropoffAddress setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMake setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtModel setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtYear setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtAddress setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtState setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtSearch setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.lblRunning.textColor = [UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    self.lblNotRunning.textColor = [UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    self.lblOpen.textColor = [UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    self.lblEnclosed.textColor = [UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    
    self.btnPickUp.titleLabel.textAlignment=NSTextAlignmentCenter;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -
#pragma mark - UIButton Action Methods

- (IBAction)onClickBtnPickUp:(id)sender
{
    if([NSPref GetBoolPreference:PREF_DEFAULT_CARD])
    {
        [self.btnMenu setTitle:NSLocalizedString(@"CREATE_REQUEST", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"CREATE_REQUEST", nil) forState:UIControlStateSelected];
        self.txtPickupAddress.text=@"";
        self.txtDropoffAddress.text=@"";
        self.txtMake.text=@"";
        self.txtModel.text=@"";
        self.txtYear.text=@"";
        self.txtState.text=@"";
        [self onClickSelectTransport:self.btnOpen];
        [self onClickSelectVehicleOperation:self.btnRunning];
        self.textViewForDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
        self.textViewForDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
        self.viewForCreateRequest.hidden=NO;
    }
    else
    {
        [[UtilityClass sharedObject] showAlertWithTitle:nil andMessage:NSLocalizedString(@"36", nil)];
    }
    
}
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
}

- (IBAction)onClickBtnPickUpLocation:(id)sender
{
    [self.view endEditing:YES];
    self.viewForDatePicker.hidden=YES;
    /* strForLongitude=strForSourceLatitude;
     strForLongitude=strForSourceLongitude;
     
     CLLocationCoordinate2D coor;
     coor.latitude=[strForLatitude doubleValue];
     coor.longitude=[strForLongitude doubleValue];
     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
     [mapView_ animateWithCameraUpdate:updatedCamera];*/
    
    address_type=@"PickUp";
    self.txtAddress.placeholder=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.imgPin.image=[UIImage imageNamed:@"pin_source"];
    self.tableForCity.hidden=YES;
    self.viewForMap.hidden=NO;
    
}
- (IBAction)onClickBtnDropOffLocation:(id)sender
{
    [self.view endEditing:YES];
    self.viewForDatePicker.hidden=YES;
    /*  strForLongitude=strForDestinationLatitude;
     strForLongitude=strForDestinationLongitude;
     
     CLLocationCoordinate2D coor;
     coor.latitude=[strForLatitude doubleValue];
     coor.longitude=[strForLongitude doubleValue];
     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
     [mapView_ animateWithCameraUpdate:updatedCamera]; */
    
    address_type=@"DropOff";
    self.txtAddress.placeholder=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.imgPin.image=[UIImage imageNamed:@"pin_destination"];
    self.tableForCity.hidden=YES;
    self.viewForMap.hidden=NO;
}

- (IBAction)onClickCloseSearchView:(id)sender
{
    self.viewForSearch.hidden=YES;
}
- (IBAction)onClickBtnPlaceRequest:(id)sender
{
    [self.view endEditing:YES];
    self.viewForDatePicker.hidden=YES;
    
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtPickupAddress.text.length<1 || self.txtDropoffAddress.text.length<1 || [self.btnSelectState.titleLabel.text isEqualToString:NSLocalizedString(@"STATE", nil)] || [self.btnYear.titleLabel.text isEqualToString:NSLocalizedString(@"YEAR", nil)] || [self.btnMake.titleLabel.text isEqualToString:NSLocalizedString(@"MAKE", nil)] || [self.btnModel.titleLabel.text isEqualToString:NSLocalizedString(@"MODEL", nil)]  || [self.btnTrim.titleLabel.text isEqualToString:NSLocalizedString(@"TRIM", nil)]|| [strForRunning isEqualToString:@""] || [strForOpen isEqualToString:@""])
        {
            if(self.txtPickupAddress.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"27", nil)];
            }
            else if (self.txtDropoffAddress.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"28", nil)];
            }
            else if ([self.btnSelectState.titleLabel.text isEqualToString:NSLocalizedString(@"STATE", nil)])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_SELECT_STATE", nil)];
            }
            else if ([self.btnYear.titleLabel.text isEqualToString:NSLocalizedString(@"YEAR", nil)])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"33", nil)];
            }
            else if ([self.btnMake.titleLabel.text isEqualToString:NSLocalizedString(@"MAKE", nil)])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"31", nil)];
            }
            else if ([self.btnModel.titleLabel.text isEqualToString:NSLocalizedString(@"MODEL", nil)])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"32", nil)];
            }
            else if ([self.btnTrim.titleLabel.text isEqualToString:NSLocalizedString(@"TRIM", nil)])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"101", nil)];
            }
            else if ([strForRunning isEqualToString:@""])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"102", nil)];
            }
            else if ([strForOpen isEqualToString:@""])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"103", nil)];
            }
            else if ([self.textViewForDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"29", nil)];
            }
        }
        else
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GET_ALERT", nil)];
            [self getAlert];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

- (IBAction)onClickBtnAddressDone:(id)sender
{
    [self.view endEditing:YES];
    if([address_type isEqualToString:@"PickUp"])
    {
        strForSourceLatitude=strForLatitude;
        strForSourceLongitude=strForLongitude;
        self.txtPickupAddress.text=self.txtAddress.text;
        strForLatitude=strForCurLatitude;
        strForLongitude=strForCurLongitude;
    }
    else
    {
        strForDestinationLatitude=strForLatitude;
        strForDestinationLongitude=strForLongitude;
        self.txtDropoffAddress.text=self.txtAddress.text;
        strForLatitude=strForCurLatitude;
        strForLongitude=strForCurLongitude;
    }
    self.viewForMap.hidden=YES;
}

- (IBAction)onClickBtnCurrentLocation:(id)sender
{
    if ([CLLocationManager locationServicesEnabled])
    {
        CLLocationCoordinate2D coor;
        coor.latitude=[strForCurLatitude doubleValue];
        coor.longitude=[strForCurLongitude doubleValue];
        strForLatitude=strForCurLatitude;
        strForLongitude=strForCurLongitude;
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
        [mapView_ animateWithCameraUpdate:updatedCamera];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"LOCATION_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
}
- (IBAction)onClickBtnDatePickCancel:(id)sender
{
    self.viewForDatePicker.hidden=YES;
}
- (IBAction)onClickBtnDatePickDone:(id)sender
{
    self.viewForDatePicker.hidden=YES;
    
    if([pickerType isEqualToString:@"state"])
    {
        if(dictForState.count>0)
        {
            self.txtState.text=[dictForState valueForKey:@"state_name"];
            strForStateId=[dictForState valueForKey:@"state_id"];
        }
    }
    else if([pickerType isEqualToString:@"year"])
    {
        [self.btnYear setTitle:strForPickerValue forState:UIControlStateNormal];
        [self.btnYear setTitle:strForPickerValue forState:UIControlStateHighlighted];
        
        if(![[self.btnYear titleForState:UIControlStateNormal]isEqualToString:NSLocalizedString(@"YEAR", nil)])
        {
            [self.btnMake setUserInteractionEnabled:YES];
        }
    }
    else if([pickerType isEqualToString:@"make"])
    {
        [self.btnMake setTitle:strForPickerValue forState:UIControlStateNormal];
        [self.btnMake setTitle:strForPickerValue forState:UIControlStateHighlighted];
        
        if(![[self.btnMake titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"MAKE", nil)])
        {
            [self.btnModel setUserInteractionEnabled:YES];
        }
    }
    else if([pickerType isEqualToString:@"model"])
    {
        [self.btnModel setTitle:strForPickerValue forState:UIControlStateNormal];
        [self.btnModel setTitle:strForPickerValue forState:UIControlStateHighlighted];
        
        if(![[self.btnModel titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"MODEL", nil)])
        {
            [self.btnTrim setUserInteractionEnabled:YES];
        }
    }
    else
    {
        [self.btnTrim setTitle:strForPickerValue forState:UIControlStateNormal];
        [self.btnTrim setTitle:strForPickerValue forState:UIControlStateHighlighted];
    }
}
- (IBAction)onClickBtnSelectState:(id)sender
{
    pickerType=@"state";
    self.txtSearch.text = @"";
    [self getState];
    [APPDELEGATE showLoadingWithTitle:@""];
    self.viewForSearch.hidden=NO;
    [searchedArray removeAllObjects];
    [searchedArray addObjectsFromArray:arrForState];
    [self.tableForVehicleDetails reloadData];
    [APPDELEGATE hideLoadingView];
}
- (IBAction)onClickBtnYear:(id)sender
{
    pickerType=@"year";
    self.txtSearch.text = @"";
    [self getYears];
}

- (IBAction)onClickBtnMake:(id)sender
{
    pickerType=@"make";
    self.txtSearch.text = @"";
    [self getMakes];
}

- (IBAction)onClickBtnModel:(id)sender
{
    pickerType = @"model";
    self.txtSearch.text = @"";
    [self getModels];
}

- (IBAction)onClickBtnTrim:(id)sender
{
    pickerType = @"trim";
    self.txtSearch.text = @"";
    [self getTrims];
    /*[APPDELEGATE showLoadingWithTitle:@""];
    self.viewForSearch.hidden=NO;
    [searchedArray removeAllObjects];
    [searchedArray addObjectsFromArray:trims];
    [self.tableForVehicleDetails performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    [APPDELEGATE hideLoadingView];*/
}

#pragma mark -
#define mark - UIPicker View Delegate methods

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView*)thePickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if([pickerType isEqualToString:@"state"])
    {
        return [arrForState count];
    }
    else if([pickerType isEqualToString:@"year"])
    {
        return [years count];
    }
    else if([pickerType isEqualToString:@"make"])
    {
        return [makes count];
    }
    else if([pickerType isEqualToString:@"model"])
    {
        return [models count];
    }
    else
    {
        return [trims count];
    }
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([pickerType isEqualToString:@"state"])
    {
        NSMutableDictionary *dictState=[arrForState objectAtIndex:row];
        return  [dictState valueForKey:@"state_name"];
    }
    else if([pickerType isEqualToString:@"year"])
    {
        return [years objectAtIndex:row];
    }
    else if([pickerType isEqualToString:@"make"])
    {
        return [makes objectAtIndex:row];
    }
    else if([pickerType isEqualToString:@"model"])
    {
        return [models objectAtIndex:row];
    }
    else
    {
        return [trims objectAtIndex:row];
    }
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([pickerType isEqualToString:@"state"])
    {
        if(arrForState.count>0)
        {
            dictForState=[arrForState objectAtIndex:row];
        }
    }
    else if([pickerType isEqualToString:@"year"])
    {
        strForPickerValue = [years objectAtIndex:row];
    }
    else if([pickerType isEqualToString:@"make"])
    {
        strForPickerValue = [makes objectAtIndex:row];
    }
    else if([pickerType isEqualToString:@"model"])
    {
        strForPickerValue = [models objectAtIndex:row];
    }
    else
    {
        strForPickerValue = [trims objectAtIndex:row];
    }
}

#pragma mark -
#pragma mark - Searching Methods

- (IBAction)searching:(id)sender
{
    aPlacemark=nil;
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    CLGeocoder *geocoder;
    
    NSString *str=self.txtAddress.text;
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        //[dictParam setObject:str forKey:PARAM_ADDRESS];
        [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
        [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
        [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 //NSArray *arrAddress=[response valueForKey:@"results"];
                 NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
                 
                 NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
                 
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                     
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     [self.tableForCity reloadData];
                     
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
                 
             }
             
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

- (IBAction)onClickSelectVehicleOperation:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(btn==self.btnRunning)
    {
        [self.btnRunning setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateNormal];
        [self.btnRunning setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateHighlighted];
        [self.btnNotRunning setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateNormal];
        [self.btnNotRunning setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateHighlighted];
        strForRunning = @"1";
    }
    else if (btn==self.btnNotRunning)
    {
        [self.btnNotRunning setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateNormal];
        [self.btnNotRunning setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateHighlighted];
        [self.btnRunning setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateNormal];
        [self.btnRunning setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateHighlighted];
        strForRunning = @"0";
    }
}

- (IBAction)onClickSelectTransport:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(btn==self.btnOpen)
    {
        [self.btnOpen setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateNormal];
        [self.btnOpen setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateHighlighted];
        [self.btnEnclosed setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateNormal];
        [self.btnEnclosed setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateHighlighted];
        strForOpen = @"1";
    }
    else if (btn==self.btnEnclosed)
    {
        [self.btnEnclosed setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateNormal];
        [self.btnEnclosed setImage:[UIImage imageNamed:@"img_selected"] forState:UIControlStateHighlighted];
        [self.btnOpen setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateNormal];
        [self.btnOpen setImage:[UIImage imageNamed:@"img_deselected"] forState:UIControlStateHighlighted];
        strForOpen = @"0";
    }
}

#pragma mark - Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.tableForVehicleDetails)
    {
        if(searchedArray.count!=0)
            return searchedArray.count;
        else
            return 1;
    }
    else
        return placeMarkArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if(tableView == self.tableForVehicleDetails)
    {
        if([pickerType isEqualToString:@"state"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strForState=[[searchedArray objectAtIndex:indexPath.row] valueForKey:@"state_name"];
                [self setText:cell.textLabel];
                cell.textLabel.text=strForState;
                UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                separatorLineView.backgroundColor = [UIColor lightGrayColor];
                
                [cell.contentView addSubview:separatorLineView];
            }
            else
            {
                cell.textLabel.text = @"None";
            }
        }
        else if([pickerType isEqualToString:@"year"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strForYear=[searchedArray objectAtIndex:indexPath.row];
                [self setText:cell.textLabel];
                cell.textLabel.text=strForYear;
                UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                separatorLineView.backgroundColor = [UIColor lightGrayColor];
                
                [cell.contentView addSubview:separatorLineView];
            }
            else
            {
                cell.textLabel.text = @"None";
            }
        }
        else if([pickerType isEqualToString:@"make"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strForMake=[[searchedArray objectAtIndex:indexPath.row] valueForKey:@"make_display"];
                if(strForMake.length>1)
                {
                    [self setText:cell.textLabel];
                    cell.textLabel.text=strForMake;
                    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                    separatorLineView.backgroundColor = [UIColor lightGrayColor];
                    
                    [cell.contentView addSubview:separatorLineView];
                }
                else
                {
                    [self setText:cell.textLabel];
                    cell.textLabel.text = @"None";
                    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                    separatorLineView.backgroundColor = [UIColor lightGrayColor];
                    
                    [cell.contentView addSubview:separatorLineView];
                }
            }
            else
            {
                cell.textLabel.text = @"None";
            }
        }
        else if([pickerType isEqualToString:@"model"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strForModel=[[searchedArray objectAtIndex:indexPath.row] valueForKey:@"model_name"];
                if(strForModel.length>1)
                {
                    [self setText:cell.textLabel];
                    cell.textLabel.text=strForModel;
                    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                    separatorLineView.backgroundColor = [UIColor lightGrayColor];
                    
                    [cell.contentView addSubview:separatorLineView];
                }
                else
                {
                    [self setText:cell.textLabel];
                    cell.textLabel.text = @"None";
                    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                    separatorLineView.backgroundColor = [UIColor lightGrayColor];
                    
                    [cell.contentView addSubview:separatorLineView];
                }
            }
            else
            {
                cell.textLabel.text = @"None";
            }
        }
        else if([pickerType isEqualToString:@"trim"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strForTrim=[[searchedArray objectAtIndex:indexPath.row] valueForKey:@"model_trim"];
                if(strForTrim.length>1)
                {
                    [self setText:cell.textLabel];
                    cell.textLabel.text=strForTrim;
                    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                    separatorLineView.backgroundColor = [UIColor lightGrayColor];
                    
                    [cell.contentView addSubview:separatorLineView];
                }
                else
                {
                    [self setText:cell.textLabel];
                    cell.textLabel.text = @"None";
                    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, cell.frame.size.width, 1)];
                    separatorLineView.backgroundColor = [UIColor lightGrayColor];
                    
                    [cell.contentView addSubview:separatorLineView];
                }
            }
            else
            {
                cell.textLabel.text = @"None";
            }
        }
    }
    else
    {
        if(placeMarkArr.count>0)
        {
            NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"];
            cell.textLabel.numberOfLines=0;
            cell.textLabel.lineBreakMode=NSLineBreakByWordWrapping;
            cell.textLabel.font=[FontStyleGuide fontRegular:15.0f];
            cell.textLabel.text=formatedAddress;
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if(tableView == self.tableForVehicleDetails)
    {
        if([pickerType isEqualToString:@"state"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strId = [NSString stringWithFormat:@"%@",[[searchedArray objectAtIndex:indexPath.row] valueForKey:@"state_id"]];
                
                if(strId.length>1)
                {
                    strForStateId=strId;
                }
                else
                {
                    strForStateId=@"None";
                }
                
                NSString *strState = [[searchedArray objectAtIndex:indexPath.row]valueForKey:@"state_name"];
                if(strState.length>1)
                {
                    [self.btnSelectState setTitle:[[searchedArray objectAtIndex:indexPath.row]valueForKey:@"state_name"] forState:UIControlStateNormal];
                    [self.btnSelectState setTitle:[[searchedArray objectAtIndex:indexPath.row]valueForKey:@"state_name"] forState:UIControlStateHighlighted];
                    self.btnSelectState.titleLabel.text = [[searchedArray objectAtIndex:indexPath.row]valueForKey:@"state_name"];
                }
                else
                {
                    [self.btnSelectState setTitle:@"None" forState:UIControlStateNormal];
                    [self.btnSelectState setTitle:@"None" forState:UIControlStateHighlighted];
                    self.btnSelectState.titleLabel.text = @"None";
                }
            }
            else
            {
                [self.btnSelectState setTitle:@"None" forState:UIControlStateNormal];
                [self.btnSelectState setTitle:@"None" forState:UIControlStateHighlighted];
                self.btnSelectState.titleLabel.text = @"None";
            }
        }
        else if([pickerType isEqualToString:@"year"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strYear = [searchedArray objectAtIndex:indexPath.row];
                {
                    if(strYear.length>0)
                    {
                        [self.btnYear setTitle:[searchedArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                        [self.btnYear setTitle:[searchedArray objectAtIndex:indexPath.row] forState:UIControlStateHighlighted];
                        self.btnYear.titleLabel.text = [searchedArray objectAtIndex:indexPath.row];
                    }
                    else
                    {
                        [self.btnYear setTitle:@"None" forState:UIControlStateNormal];
                        [self.btnYear setTitle:@"None" forState:UIControlStateHighlighted];
                        self.btnYear.titleLabel.text = @"None";
                    }
                }
            }
            else
            {
                [self.btnYear setTitle:@"None" forState:UIControlStateNormal];
                [self.btnYear setTitle:@"None" forState:UIControlStateHighlighted];
                self.btnYear.titleLabel.text = @"None";
            }
            
            [self.btnMake setUserInteractionEnabled:YES];
            pickerType = @"make";
            
            //[self getMakes];
        }
        else if([pickerType isEqualToString:@"make"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strId = [NSString stringWithFormat:@"%@",[[searchedArray objectAtIndex:indexPath.row] valueForKey:@"make_id"]];
                
                // check that selected data is null or not
                
                if(strId.length>1)
                {
                    strMakeId=strId;
                }
                else
                {
                    strMakeId=@"None";
                }
                
                NSString *str = [NSString stringWithFormat:@"%@",[[searchedArray objectAtIndex:indexPath.row]valueForKey:@"make_id"]];
                if(str.length>1)
                {
                    [self.btnMake setTitle:str forState:UIControlStateNormal];
                    [self.btnMake setTitle:str forState:UIControlStateHighlighted];
                    self.btnMake.titleLabel.text = str;
                }
                else
                {
                    [self.btnMake setTitle:@"None" forState:UIControlStateNormal];
                    [self.btnMake setTitle:@"None" forState:UIControlStateHighlighted];
                    self.btnMake.titleLabel.text = @"None";
                }
            }
            else
            {
                [self.btnMake setTitle:@"None" forState:UIControlStateNormal];
                [self.btnMake setTitle:@"None" forState:UIControlStateHighlighted];
                self.btnMake.titleLabel.text = @"None";
            }
            
            [self.btnModel setUserInteractionEnabled:YES];
            
            pickerType = @"model";
            
            //[self getModels];
        }
        else if([pickerType isEqualToString:@"model"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strModel = [[searchedArray objectAtIndex:indexPath.row]valueForKey:@"model_name"];
                if(strModel.length>1)
                {
                    [self.btnModel setTitle:strModel forState:UIControlStateNormal];
                    [self.btnModel setTitle:strModel forState:UIControlStateHighlighted];
                    self.btnModel.titleLabel.text = strModel;
                }
                else
                {
                    [self.btnModel setTitle:@"None" forState:UIControlStateNormal];
                    [self.btnModel setTitle:@"None" forState:UIControlStateHighlighted];
                    self.btnModel.titleLabel.text = @"None";
                }
            }
            else
            {
                [self.btnModel setTitle:@"None" forState:UIControlStateNormal];
                [self.btnModel setTitle:@"None" forState:UIControlStateHighlighted];
                self.btnModel.titleLabel.text = @"None";
            }
            
            [self.btnTrim setUserInteractionEnabled:YES];
            
            pickerType = @"trim";
            
            //[self getTrims];
        }
        else if([pickerType isEqualToString:@"trim"])
        {
            if(searchedArray.count!=0)
            {
                NSString *strTrim = [[searchedArray objectAtIndex:indexPath.row] valueForKey:@"model_trim"];
                if(strTrim.length>1)
                {
                    [self.btnTrim setTitle:strTrim forState:UIControlStateNormal];
                    [self.btnTrim setTitle:strTrim forState:UIControlStateHighlighted];
                    self.btnTrim.titleLabel.text = strTrim;
                }
                else
                {
                    [self.btnTrim setTitle:@"None" forState:UIControlStateNormal];
                    [self.btnTrim setTitle:@"None" forState:UIControlStateHighlighted];
                    self.btnTrim.titleLabel.text = @"None";
                }
            }
            else
            {
                [self.btnTrim setTitle:@"None"  forState:UIControlStateNormal];
                [self.btnTrim setTitle:@"None"  forState:UIControlStateHighlighted];
                self.btnTrim.titleLabel.text = @"None";
            }
        }
    }
    else
    {
        is_select=YES;
        aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
        self.tableForCity.hidden=YES;
        [self setNewPlaceData];
    }
    
    self.viewForSearch.hidden=YES;
}
-(void)setNewPlaceData
{
    self.txtAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
    [self textFieldShouldReturn:self.txtAddress];
}

#pragma mark -
#pragma mark - UITextView Delegate Methods

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.viewForDatePicker.hidden=YES;
    if([self.textViewForDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
    {
        self.textViewForDescription.text=@"";
        self.textViewForDescription.textColor=[UIColor blackColor];
    }
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
        self.textViewForDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    }
    return YES;
}
#pragma mark -
#pragma mark - UITextField Delegate Methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([pickerType isEqualToString:@"year"])
    {
        if(textField==self.txtSearch)
        {
            NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}
-(void)textFieldDidChange:(UITextField*)textField
{
    searchTextString = textField.text;
    [self updateSearchArray];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([pickerType isEqualToString:@"year"])
    {
        [self.txtSearch setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    }
    else
    {
        [self.txtSearch setKeyboardType:UIKeyboardTypeDefault];
    }
    self.viewForDatePicker.hidden=YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        [self getLocationFromString:self.txtAddress.text];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        [self.txtAddress resignFirstResponder];
    }
    self.tableForCity.hidden=YES;
    return YES;
}
-(void)getLocationFromString:(NSString *)str
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:str forKey:PARAM_ADDRESS];
        [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 NSArray *arrAddress=[response valueForKey:@"results"];
                 
                 if ([arrAddress count] > 0)
                     
                 {
                     //self.txtAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     
                     strForLatitude=[dictLocation valueForKey:@"lat"];
                     strForLongitude=[dictLocation valueForKey:@"lng"];
                     CLLocationCoordinate2D coor;
                     coor.latitude=[strForLatitude doubleValue];
                     coor.longitude=[strForLongitude doubleValue];
                     
                     
                     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
                     [mapView_ animateWithCameraUpdate:updatedCamera];
                 }
                 
             }
             
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - UICollectionView Delegate Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            self.viewForCreateRequest.hidden=YES;
            self.viewForMap.hidden=YES;
            [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"HOME", nil) forState:UIControlStateSelected];
            [self onClickBtnMenu:self];
            break;
        case 1:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 3:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)LogoutService
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [self RemovePreferences];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [NSPref RemovePreference:PREF_REQUEST_ID];
    [NSPref RemovePreference:PREF_REQUEST_DESCRIPTION];
    [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
    [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
    [NSPref RemovePreference:PREF_VEHICLE_MAKE];
    [NSPref RemovePreference:PREF_VEHICLE_MODEL];
    [NSPref RemovePreference:PREF_VEHICLE_YEAR];
    [NSPref RemovePreference:PREF_REQUEST_ACCEPT];
    [NSPref RemovePreference:PREF_DOWNPAYMENT];
    [NSPref RemovePreference:PREF_BID_PRICE];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - CLLocationManager Delegate Methods

- (IBAction)getCurrentLocation:(id)sender
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        strForCurLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        strForCurLongitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    }
}
#pragma mark -
#pragma mark- Google Map Delegate

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    strForLatitude=[NSString stringWithFormat:@"%f",position.target.latitude];
    strForLongitude=[NSString stringWithFormat:@"%f",position.target.longitude];
}

- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if(is_select)
    {
        is_select=NO;
    }
    else
    {
        [self getAddress];
    }
}
-(void)getAddress
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        if (getAddress.count!=0)
        {
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
    else if (alertView.tag==200)
    {
        if(buttonIndex==1)
        {
            [self LogoutService];
        }
    }
    else if (alertView.tag==300)
    {
        if(buttonIndex==1)
        {
            [self createRequest];
        }
        else
        {
            [self SetLocalization];
            self.viewForCreateRequest.hidden=YES;
            strForStateId = @"";
            strMakeId = @"";
            [self onClickSelectTransport:self.btnOpen];
            [self onClickSelectVehicleOperation:self.btnRunning];
        }
    }
}

#pragma mark -
#pragma mark - GetState Methods

-(void)getState
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:P_GET_STATE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [arrForState removeAllObjects];
                     [arrForState addObjectsFromArray:[response valueForKey:@"state_data"]];
                     [searchedArray removeAllObjects];
                     [searchedArray addObjectsFromArray:arrForState];
                     if(arrForState.count>0)
                     {
                         dictForState=[arrForState objectAtIndex:0];
                     }
                     [APPDELEGATE hideLoadingView];
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark -

-(void)getAllMyCards
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GETTING_CARDS", nil)];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",P_GET_CARDS,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN]];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"All Card = %@",response);
             if (response)
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSMutableArray *arrForCards=[[NSMutableArray alloc] init];
                     [arrForCards addObjectsFromArray:[response valueForKey:@"payments"]];
                     if(arrForCards.count==0)
                     {
                         
                         [NSPref SetBoolPreference:PREF_DEFAULT_CARD Value:0];
                     }
                     else
                     {
                         [NSPref SetBoolPreference:PREF_DEFAULT_CARD Value:1];
                     }
                 }
                 else
                 {
                     
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

@end
