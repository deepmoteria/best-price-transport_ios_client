//
//  PickUpVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 26/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface PickUpVC : BaseVC <UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UIAlertViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    GMSMapView *mapView_;
}
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (weak, nonatomic) IBOutlet UILabel *lblwelcome2;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblRunning;
@property (weak, nonatomic) IBOutlet UILabel *lblNotRunning;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblEnclosed;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;


@property (weak, nonatomic) IBOutlet UIButton *btnPickUp;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnPlaceRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectState;
@property (weak, nonatomic) IBOutlet UIButton *btnMake;
@property (weak, nonatomic) IBOutlet UIButton *btnModel;
@property (weak, nonatomic) IBOutlet UIButton *btnTrim;
@property (weak, nonatomic) IBOutlet UIButton *btnRunning;
@property (weak, nonatomic) IBOutlet UIButton *btnNotRunning;
@property (weak, nonatomic) IBOutlet UIButton *btnOpen;
@property (weak, nonatomic) IBOutlet UIButton *btnEnclosed;
@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForCreateRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForMap;

@property (weak, nonatomic) IBOutlet UIView *viewForSearch;

@property (weak, nonatomic) IBOutlet UIView *mapView;

@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;

@property (weak, nonatomic) IBOutlet UITextView *textViewForMessage;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtPickupAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtDropoffAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtMake;
@property (weak, nonatomic) IBOutlet UITextField *txtModel;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

@property (weak, nonatomic) IBOutlet UIScrollView *ScrollObj;
@property (weak, nonatomic) IBOutlet UITableView *tableForCity;
@property (weak, nonatomic) IBOutlet UITableView *tableForVehicleDetails;

@property (weak, nonatomic) IBOutlet UIImageView *imgWelcome;
@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (weak, nonatomic) IBOutlet UIImageView *imgPin;

- (IBAction)onClickBtnPickUp:(id)sender;
- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnPickUpLocation:(id)sender;
- (IBAction)onClickBtnDropOffLocation:(id)sender;

- (IBAction)onClickCloseSearchView:(id)sender;

- (IBAction)onClickBtnPlaceRequest:(id)sender;
- (IBAction)onClickBtnAddressDone:(id)sender;

- (IBAction)onClickBtnCurrentLocation:(id)sender;
- (IBAction)onClickBtnSelectState:(id)sender;

- (IBAction)searching:(id)sender;
- (IBAction)onClickSelectVehicleOperation:(id)sender;
- (IBAction)onClickSelectTransport:(id)sender;


// forDatePicker
@property (weak, nonatomic) IBOutlet UIView *viewForDatePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerObj;


@property (weak, nonatomic) IBOutlet UIButton *btnDatePickDone;
@property (weak, nonatomic) IBOutlet UIButton *btnDatePickCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnYear;

- (IBAction)onClickBtnDatePickCancel:(id)sender;
- (IBAction)onClickBtnDatePickDone:(id)sender;
- (IBAction)onClickBtnYear:(id)sender;
- (IBAction)onClickBtnMake:(id)sender;
- (IBAction)onClickBtnModel:(id)sender;
- (IBAction)onClickBtnTrim:(id)sender;





@end
