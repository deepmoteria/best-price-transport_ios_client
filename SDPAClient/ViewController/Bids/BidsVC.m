//
//  BidsVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 02/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.

#import "BidsVC.h"
#import "MenuCollectionViewCell.h"
#import "PickUpVC.h"
#import "BidCell.h"

@interface BidsVC ()
{
     NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier,*arrForBids;
     CGRect originalFrameOfBid;
    NSInteger RejectButtonTag,AcceptButtonTag;
    NSTimer *TimerForGetBid,*timerForPush;
}
@end

@implementation BidsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForSegueIdentifier=[[NSMutableArray alloc] init];
    arrForBids=[[NSMutableArray alloc] init];
    
    originalFrameOfBid=self.viewForBids.frame;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    [self SetData];
    NSString *strReqId=[NSPref GetPreference:PREF_REQUEST_ID];
    if(strReqId!=nil)
    {
        if([NSPref GetBoolPreference:PREF_REQUEST_ACCEPT])
        {
            [self performSegueWithIdentifier:SEGUE_TO_BID_SUCCESS sender:self];
        }
        else
        {
            [self GetBidList];
            TimerForGetBid=[NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(GetBidList) userInfo:nil repeats:YES];
        }
    }
    self.viewForAcceptBid.hidden=YES;
    self.viewForCarrierNote.hidden=YES;
    
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_HOME", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_PAYMENT", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_payment",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc] initWithObjects:@"Home",SEGUE_TO_PROFILE,SEGUE_TO_PAYMENT,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"logout", nil];
    self.viewObj=nil;
    for (int i=0; i<self.navigationController.viewControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if([vc isKindOfClass:[PickUpVC class]])
        {
            self.viewObj=(PickUpVC *)vc;
        }
    }
    
    [timerForPush invalidate];
    timerForPush=nil;
    timerForPush=[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(CheckPush) userInfo:nil repeats:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [TimerForGetBid invalidate];
    TimerForGetBid=nil;
    
   // [UIView animateWithDuration:0.5 animations:^{
        [self.btnMenu setTitle:NSLocalizedString(@"BIDS", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"BIDS", nil) forState:UIControlStateSelected];
        [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
 //   } completion:^(BOOL finished)
 //    {
         
 //    }];
    
}
-(void)CheckPush
{
    if(pushId==1)
    {
        [self GetBidList];
        pushId=0;
    }
}
-(void)SetData
{
    self.btnHideShowDetail.tag=0;
    self.lblVehicleYear.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[NSPref GetPreference:PREF_VEHICLE_MAKE],[NSPref GetPreference:PREF_VEHICLE_MODEL],[NSPref GetPreference:PREF_VEHICLE_YEAR],[NSPref GetPreference:PREF_VEHICLE_TRIM]];
    self.lblPickupAddress.text=[NSPref GetPreference:PREF_SOURCE_ADDRESS];
    self.lblDropoffAddress.text=[NSPref GetPreference:PREF_DESTINATION_ADDRESS];
    if([NSPref GetBoolPreference:PREF_IS_RUNNING])
        self.lblSetOperational.text = NSLocalizedString(@"RUNNING", nil);
    else
        self.lblSetOperational.text = NSLocalizedString(@"NOT_RUNNING", nil);
    
    if([NSPref GetBoolPreference:PREF_IS_TRANSPORT])
        self.lblSetTransport.text = NSLocalizedString(@"OPEN", nil);
    else
        self.lblSetTransport.text = NSLocalizedString(@"ENCLOSED", nil);
    
    self.textViewForDescription.text=[NSPref GetPreference:PREF_REQUEST_DESCRIPTION];
    
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
    }
    
    NSString *strDate = [NSPref GetPreference:PREF_EXPIRY_DATE];
    
    strDate = [self OnlyDateConverter:strDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    
    NSString *strCurrentDate = [formatter stringFromDate:[NSDate date]];
    
    if([strDate isEqualToString:strCurrentDate])
    {
        NSLog(@"equal");
        [self.btnRelistRequest setUserInteractionEnabled:YES];
        [self.btnRelistRequest setAlpha:1.0];
    }
    else
    {
        NSLog(@"Notequal");
        [self.btnRelistRequest setUserInteractionEnabled:NO];
        [self.btnRelistRequest setAlpha:0.7];
    }
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    self.lblVehicle.text=NSLocalizedString(@"VEHICLE", nil);
    self.lblPickup.text=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.lblDropoff.text=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblTitleOfBidView.text=NSLocalizedString(@"CARRIER_BID", nil);
    self.lblCarrierViewTitle.text=NSLocalizedString(@"CARRIER_NOTE", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    
    self.lblDownpayment.text=NSLocalizedString(@"DOWN_PAYMENT_MSG", nil);
    self.lblDownpaymentDetail.text=NSLocalizedString(@"DOWN_PAYMENT_DETAIL_MSG", nil);
    self.lblRemainPayment.text=NSLocalizedString(@"REMAIN_PAYMENT_MSG", nil);
    self.lblRemainPaymentDetail.text=NSLocalizedString(@"REMAIN_PAYMENT_DETAIL_MSG", nil);
    self.lblTotalPayment.text=NSLocalizedString(@"TOTAL_PAYMENT_MSG", nil);
    self.lblTotalPaymentDetail.text=NSLocalizedString(@"TOTAL_PAYMENT_DETAIL_MSG", nil);
    
    [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateNormal];
    [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateSelected];
    [self.btnMenu setTitle:NSLocalizedString(@"BIDS",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"BIDS",nil) forState:UIControlStateSelected];
    
    [self.btnAcceptBidClose setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateNormal];
    [self.btnAcceptBidClose setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateSelected];
    [self.btnRelistRequest setTitle:NSLocalizedString(@"RELIST_REQUEST",nil) forState:UIControlStateNormal];
    [self.btnRelistRequest setTitle:NSLocalizedString(@"RELIST_REQUEST",nil) forState:UIControlStateSelected];
    
    [self.btnConfirmPayment setTitle:NSLocalizedString(@"CONFIRM_PAYMENT",nil) forState:UIControlStateNormal];
    [self.btnConfirmPayment setTitle:NSLocalizedString(@"CONFIRM_PAYMENT",nil) forState:UIControlStateSelected];
    [self.btnCarrierViewClose setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateNormal];
    [self.btnCarrierViewClose setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateSelected];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UICollectionView Delegate Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            [self.btnMenu setTitle:NSLocalizedString(@"BIDS", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"BIDS", nil) forState:UIControlStateSelected];
            [self onClickBtnMenu:self];
            break;
        case 1:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 3:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)LogoutService
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [self RemovePreferences];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [NSPref RemovePreference:PREF_REQUEST_ID];
    [NSPref RemovePreference:PREF_REQUEST_DESCRIPTION];
    [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
    [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
    [NSPref RemovePreference:PREF_VEHICLE_MAKE];
    [NSPref RemovePreference:PREF_VEHICLE_MODEL];
    [NSPref RemovePreference:PREF_VEHICLE_YEAR];
    [NSPref RemovePreference:PREF_REQUEST_ACCEPT];
    [NSPref RemovePreference:PREF_DOWNPAYMENT];
    [NSPref RemovePreference:PREF_BID_PRICE];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self DeleteRequest];
                break;
            default:
                break;
        }
    }
    else if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self LogoutService];
                break;
                
            default:
                break;
        }
    }
    else if(alertView.tag==300)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self RejectBidService];
                break;
                
            default:
                break;
        }
    }
    else if(alertView.tag==10)
    {
        if (buttonIndex==0)
        {
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:2] sender:self];
        }
    }
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark -
#pragma mark - UIButton Actions Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"BIDS", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"BIDS", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
}
- (IBAction)onClickBtnDelete:(id)sender
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"DELETE_REQUEST_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=100;
    [alert show];
}

- (IBAction)onClickBtnHideShowDetails:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateNormal];
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForDetails.hidden=YES;
            self.viewForBids.frame=CGRectMake(self.viewForBids.frame.origin.x, self.viewForDetails.frame.origin.y, self.viewForBids.frame.size.width,self.view.frame.size.height-self.lbl.frame.size.height-self.lbl.frame.origin.y-15);
        } completion:^(BOOL finished)
         {
         }];
    }
    else
    {
        btn.tag=0;
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateNormal];
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.viewForBids.frame=originalFrameOfBid;
        } completion:^(BOOL finished)
         {
             self.viewForDetails.hidden=NO;
         }];
    }
}

- (IBAction)onClickRelistRequest:(id)sender
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[NSPref GetPreference:PREF_EXPIRY_DATE] forKey:PARAM_EXPIRE_TIME];
        [dictParam setObject:[NSPref GetPreference:PREF_REQUEST_ID] forKey:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_EXPIRY_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [NSPref SetPreference:PREF_EXPIRY_DATE Value:[response valueForKey:@"exp_time"]];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}
- (IBAction)onClickBtnAcceptBidClose:(id)sender
{
    self.viewForAcceptBid.hidden=YES;
}
- (IBAction)onClickBtnCarrierViewClose:(id)sender
{
    self.viewForCarrierNote.hidden=YES;
}
- (IBAction)onClickBtnConfirmPayment:(id)sender
{
    NSMutableDictionary *dict=[arrForBids objectAtIndex:AcceptButtonTag];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[dict valueForKey:@"bidding_id"] forKey:PARAM_BID_ID];
        [dictParam setObject:@"1" forKey:PARAM_ACCEPT];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_RESPONSE_TO_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"BID_ACCEPT_SUCCESS", nil)];
                     [TimerForGetBid invalidate];
                     TimerForGetBid=nil;
                     [NSPref SetBoolPreference:PREF_REQUEST_ACCEPT Value:1];
                     [self performSegueWithIdentifier:SEGUE_TO_BID_SUCCESS sender:self];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str intValue]==102)
                     {
                         UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"strip_msg"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         alert.tag=10;
                         [alert show];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}
#pragma mark -
#pragma mark - WebService Methods
-(void)DeleteRequest
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"DELETE_REQUESTING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[NSPref GetPreference:PREF_REQUEST_ID] forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_DELETE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"DELETE_REQUEST_SUCCESS", nil)];
                     [NSPref RemovePreference:PREF_REQUEST_ID];
                     [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
                     [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
                     [NSPref RemovePreference:PREF_VEHICLE_MAKE];
                     [NSPref RemovePreference:PREF_VEHICLE_MODEL];
                     [NSPref RemovePreference:PREF_VEHICLE_YEAR];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)GetBidList
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        //[[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"GETTING_BID", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_INCOMING_BID_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [NSPref SetPreference:PREF_DOWNPAYMENT Value:[response valueForKey:@"broker_fee"]];
                     [arrForBids removeAllObjects];
                     [arrForBids addObjectsFromArray:[response valueForKey:@"incoming_bidding_request"]];
                     if(arrForBids.count>0)
                     {
                         
                     }
                     else
                     {
                         self.viewForAcceptBid.hidden=YES;
                     }
                     [self.tableForBids reloadData];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)RejectBidService
{
    NSMutableDictionary *dict=[arrForBids objectAtIndex:RejectButtonTag];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[dict valueForKey:@"bidding_id"] forKey:PARAM_BID_ID];
        [dictParam setObject:@"2" forKey:PARAM_ACCEPT];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_RESPONSE_TO_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"BID_REJECT_SUCCESS", nil)];
                     [arrForBids removeAllObjects];
                     [arrForBids addObjectsFromArray:[response valueForKey:@"incoming_bidding_request"]];
                     [self.tableForBids reloadData];
                     if(arrForBids.count<1)
                     {
                         self.viewForAcceptBid.hidden=YES;
                     }
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForBids.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BidCell *cell=(BidCell *)[self.tableForBids dequeueReusableCellWithIdentifier:@"BidCell" forIndexPath:indexPath];
    
    NSMutableDictionary *dictData=[arrForBids objectAtIndex:indexPath.row];
    
    cell.lbl_price.text=[NSString stringWithFormat:@"$ %.2f",([[dictData valueForKey:@"bid_price"] floatValue]+[[NSPref GetPreference:PREF_DOWNPAYMENT] floatValue])];
    
    NSString *strForPickupTime = [dictData valueForKey:@"pickup_time"];
    
    NSString *strForPickupDate = [dictData valueForKey:@"pickup_date"];
    
    NSString *strForDeliveryTime = [dictData valueForKey:@"delivery_time"];
    
    if(![strForPickupDate isEqualToString:@"0000-00-00"])
    {
        if(![strForPickupTime isEqualToString:@"00:00:00"])
        {
            cell.lbl_time.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter:[NSString stringWithFormat:@"%@ %@",[dictData valueForKey:@"pickup_date"],[dictData valueForKey:@"pickup_time"]]]];
        }
        else
        {
            cell.lbl_time.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter2:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"pickup_date"]]]];
        }
    }
    else
    {
        if(![strForPickupTime isEqualToString:@"00:00:00"])
        {
            cell.lbl_time.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter1:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"pickup_time"]]]];
        }
        else
        {
            cell.lbl_time.text=[NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"PICK_UP_TIME", nil)];
        }
    }
    
    if(![strForDeliveryTime isEqualToString:@"0000-00-00 00:00:00"])
    {
        cell.lblDeliveryDate.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"DELIVERY_DATE", nil),[self DateConverter:[dictData valueForKey:@"delivery_time"]]];
    }
    else
    {
        cell.lblDeliveryDate.text=[NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"DELIVERY_DATE", nil)];
    }
    
    cell.textViewForNote.text=[dictData valueForKey:@"description"];
    
    if(cell.textViewForNote.text.length<1)
    {
        cell.textViewForNote.text=NSLocalizedString(@"CARRIER_NOT_FIND", nil);
    }
    
   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    if([[dictData valueForKey:@"minute"] intValue]>0)
    {
        [dateComponents setMinute:+[[dictData valueForKey:@"minute"] intValue]];
    }
    if([[dictData valueForKey:@"second"] intValue]>0)
    {
        [dateComponents setSecond:+[[dictData valueForKey:@"second"] intValue]];
    }
    NSDate *Remain_Time_Date = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    cell.lbl_remain_time.text=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"ACCEPT_BID_BEFORE", nil),[dateFormatter stringFromDate:Remain_Time_Date]];
    
    NSMutableDictionary *dict=[dictData valueForKey:@"bidding_data"];
    NSMutableDictionary *CarrierData=[dict valueForKey:@"carrier"];
    [cell.img_profile applyRoundedCornersFull];
    [cell.img_profile downloadFromURL:[CarrierData valueForKey:@"picture"] withPlaceholder:nil];
    cell.lbl_name.text=[CarrierData valueForKey:@"name"];
    cell.lbl_rate.text=[CarrierData valueForKey:@"rating"];
    
    CGFloat width=cell.lbl_name.intrinsicContentSize.width+10;
    
    if(width<cell.lbl_name.frame.size.width)
    {
        CGRect rateFrame=cell.viewForRate.frame;
        rateFrame.origin.x=cell.lbl_name.frame.origin.x+width;
        cell.viewForRate.frame=rateFrame;
    }
    
    cell.btn_viewNote.tag=indexPath.row;
    [cell.btn_viewNote addTarget:self action:@selector(ViewNote:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_Accept.tag=indexPath.row;
    [cell.btn_Accept addTarget:self action:@selector(AcceptBid:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_Reject.tag=indexPath.row;
    [cell.btn_Reject addTarget:self action:@selector(RejectBid:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark -
#pragma mark - BID Accept Reject methods

-(void)AcceptBid :(id)sender
{
    UIButton *btn=(UIButton *)sender;
    AcceptButtonTag=btn.tag;
    if(arrForBids.count>0)
    {
        NSMutableDictionary *dict=[arrForBids objectAtIndex:btn.tag];
        
        [NSPref SetPreference:PREF_BID_PRICE Value:[dict valueForKey:@"bid_price"]];
        
        self.lblDownpaymentPrice.text=[NSString stringWithFormat:@"$ %.2f",[[NSPref GetPreference:PREF_DOWNPAYMENT] floatValue]];
        self.lblRemainPaymentPrice.text=[NSString stringWithFormat:@"$ %.2f",[[dict valueForKey:@"bid_price"] floatValue]];
        self.lblTotalPaymentPrice.text=[NSString stringWithFormat:@"$ %.2f",([[dict valueForKey:@"bid_price"] floatValue]+[[NSPref GetPreference:PREF_DOWNPAYMENT] floatValue])];
        self.viewForAcceptBid.hidden=NO;
    }
    
}
-(void)RejectBid :(id)sender
{
    UIButton *btn=(UIButton *)sender;
    RejectButtonTag=btn.tag;
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"REJECT_BID_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=300;
    [alert show];
}
-(void)ViewNote :(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    self.viewForCarrierNote.hidden=NO;
    NSMutableDictionary *dict=[arrForBids objectAtIndex:btn.tag];
    self.textViewForCarrierNote.text=[dict valueForKey:@"description"];
    self.textViewForCarrierNote.textAlignment=NSTextAlignmentCenter;
    if(self.textViewForCarrierNote.text.length<1)
    {
        self.textViewForCarrierNote.text=NSLocalizedString(@"CARRIER_NOT_FIND", nil);
    }
}

#pragma mark -
#pragma mark - Helper Methods

-(NSString *)TimeConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter2:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    //  dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)OnlyDateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"yyyy-MM-dd"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}


@end