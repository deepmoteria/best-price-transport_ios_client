//
//  FeedBackVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 16/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import "PickUpVC.h"
#import "RatingBar.h"

@interface FeedBackVC : BaseVC <UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate,UITextViewDelegate>


@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (strong,nonatomic) NSMutableDictionary *CarrierDetails;
@property (strong,nonatomic) NSMutableDictionary *BiddingData;
@property (weak, nonatomic) PickUpVC *viewObj;

@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForDetails;
@property (weak, nonatomic) IBOutlet UIView *viewForRate;
@property (weak, nonatomic) IBOutlet UIView *viewRate;

@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;

@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicle;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPickup;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoff;
@property (weak, nonatomic) IBOutlet UILabel *lblDropoffAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblSetOperational;
@property (weak, nonatomic) IBOutlet UILabel *lblSetTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDate;


@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnHideShowDetail;


- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnHideShowDetails:(id)sender;

// scroll View outlet
@property (weak, nonatomic) IBOutlet UIScrollView *scrollObj;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UITextView *textViewForFeedBackDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblCarrierName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLiacencePlateNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;
@property (weak, nonatomic) IBOutlet UILabel *lblCarrierDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblRateYourCarrier;
@property (weak, nonatomic) IBOutlet UILabel *lblFeedBackDescription;

@property (weak, nonatomic) IBOutlet UIButton *btnFeedBack;

- (IBAction)OnClickBtnFeedBack:(id)sender;








@end
