//
//  FeedBackVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 16/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "FeedBackVC.h"
#import "MenuCollectionViewCell.h"

@interface FeedBackVC ()
{
    NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier;
    CGRect originalFrameOfRateView;
}
@end

@implementation FeedBackVC
@synthesize CarrierDetails,BiddingData;

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForSegueIdentifier=[[NSMutableArray alloc] init];
    originalFrameOfRateView=self.viewForRate.frame;
    [self.ratingView initRateBar];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    self.viewForDetails.hidden=YES;
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_HOME", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_PAYMENT", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_payment",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc] initWithObjects:@"Home",SEGUE_TO_PROFILE,SEGUE_TO_PAYMENT,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"logout", nil];
    
    self.viewObj=nil;
    for (int i=0; i<self.navigationController.viewControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if([vc isKindOfClass:[PickUpVC class]])
        {
            self.viewObj=(PickUpVC *)vc;
        }
    }
    
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    self.scrollObj.contentSize=CGSizeMake(self.viewForRate.frame.size.width, 470);
    self.textViewForFeedBackDescription.text=NSLocalizedString(@"FEEDBACK_NOTE_MSG", nil);
    self.textViewForFeedBackDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    
    
    self.lblVehicle.text=NSLocalizedString(@"VEHICLE", nil);
    self.lblPickup.text=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.lblDropoff.text=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblFeedBackDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblCarrierDetail.text=NSLocalizedString(@"CARRIER_DATAIL_TITLE", nil);
    self.lblRateYourCarrier.text=NSLocalizedString(@"RATE_YOUR_CARRIER", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    
    [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_COMPLETED",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_COMPLETED",nil) forState:UIControlStateSelected];
    [self.btnFeedBack setTitle:NSLocalizedString(@"SUBMIT",nil) forState:UIControlStateNormal];
    [self.btnFeedBack setTitle:NSLocalizedString(@"SUBMIT",nil) forState:UIControlStateSelected];
    
    [self SetData];
}
-(void)SetData
{
    // for detailView
    self.btnHideShowDetail.tag=0;
    self.lblVehicleYear.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[NSPref GetPreference:PREF_VEHICLE_MAKE],[NSPref GetPreference:PREF_VEHICLE_MODEL],[NSPref GetPreference:PREF_VEHICLE_YEAR],[NSPref GetPreference:PREF_VEHICLE_TRIM]];
    self.lblPickupAddress.text=[NSPref GetPreference:PREF_SOURCE_ADDRESS];
    self.lblDropoffAddress.text=[NSPref GetPreference:PREF_DESTINATION_ADDRESS];
    if([NSPref GetBoolPreference:PREF_IS_RUNNING])
        self.lblSetOperational.text = NSLocalizedString(@"RUNNING", nil);
    else
        self.lblSetOperational.text = NSLocalizedString(@"NOT_RUNNING", nil);
    
    if([NSPref GetBoolPreference:PREF_IS_TRANSPORT])
        self.lblSetTransport.text = NSLocalizedString(@"OPEN", nil);
    else
        self.lblSetTransport.text = NSLocalizedString(@"ENCLOSED", nil);
    self.textViewForDescription.text=[NSPref GetPreference:PREF_REQUEST_DESCRIPTION];
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
    }
    self.viewForDetails.hidden=YES;
    [self onClickBtnHideShowDetails:self.btnHideShowDetail];
    
    [self.imgProfile applyRoundedCornersFull];
    [self.imgProfile downloadFromURL:[CarrierDetails valueForKey:@"picture"] withPlaceholder:nil];
    self.lblCarrierName.text=[CarrierDetails valueForKey:@"name"];
    
    
    NSString *strForPickupTime = [BiddingData valueForKey:@"pickup_time"];
    
    NSString *strForPickupDate = [BiddingData valueForKey:@"pickup_date"];
    
    NSString *strForDeliveryTime = [BiddingData valueForKey:@"delivery_time"];
    
    if(![strForPickupDate isEqualToString:@"0000-00-00"])
    {
        if(![strForPickupTime isEqualToString:@"00:00:00"])
        {
            self.lblPickupTime.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter:[NSString stringWithFormat:@"%@ %@",[BiddingData valueForKey:@"pickup_date"],[BiddingData valueForKey:@"pickup_time"]]]];
        }
        else
        {
            self.lblPickupTime.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter2:[NSString stringWithFormat:@"%@",[BiddingData valueForKey:@"pickup_date"]]]];
        }
    }
    else
    {
        if(![strForPickupTime isEqualToString:@"00:00:00"])
        {
            self.lblPickupTime.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter1:[NSString stringWithFormat:@"%@",[BiddingData valueForKey:@"pickup_time"]]]];
        }
        else
        {
            self.lblPickupTime.text=[NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"PICK_UP_TIME", nil)];
        }
    }
    
    if(![strForDeliveryTime isEqualToString:@"0000-00-00 00:00:00"])
    {
        self.lblDeliveryDate.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"DELIVERY_DATE", nil),[self DateConverter:[NSString stringWithFormat:@"%@",[BiddingData valueForKey:@"delivery_time"]]]];
    }
    else
    {
        self.lblDeliveryDate.text=[NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"DELIVERY_DATE", nil)];
    }
    
    self.lblLiacencePlateNumber.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"MC_NUMBER", nil),[CarrierDetails valueForKey:@"mc_license"]];
    self.lblRate.text=[CarrierDetails valueForKey:@"rating"];
    
    CGFloat width=self.lblCarrierName.intrinsicContentSize.width+10;
    if(width<self.lblCarrierName.frame.size.width)
    {
        CGRect rateFrame=self.viewRate.frame;
        rateFrame.origin.x=self.lblCarrierName.frame.origin.x+width;
        self.viewRate.frame=rateFrame;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark -
#pragma mark - UICollectionView Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_COMPLETED", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_COMPLETED", nil) forState:UIControlStateSelected];
            [self onClickBtnMenu:self];
            break;
        case 1:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 3:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [NSPref RemovePreference:PREF_REQUEST_ID];
    [NSPref RemovePreference:PREF_REQUEST_DESCRIPTION];
    [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
    [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
    [NSPref RemovePreference:PREF_VEHICLE_MAKE];
    [NSPref RemovePreference:PREF_VEHICLE_MODEL];
    [NSPref RemovePreference:PREF_VEHICLE_YEAR];
    [NSPref RemovePreference:PREF_REQUEST_ACCEPT];
    [NSPref RemovePreference:PREF_DOWNPAYMENT];
    [NSPref RemovePreference:PREF_BID_PRICE];
    [NSPref RemovePreference:PREF_EXPIRY_DATE];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self RemovePreferences];
                break;
                
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark - UIButton Actions Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_COMPLETED", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_COMPLETED", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
}

- (IBAction)onClickBtnHideShowDetails:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateNormal];
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForDetails.hidden=YES;
            self.viewForRate.frame=CGRectMake(self.viewForRate.frame.origin.x, self.viewForDetails.frame.origin.y, self.viewForRate.frame.size.width,self.view.frame.size.height-self.lbl.frame.size.height-self.lbl.frame.origin.y-15);
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        btn.tag=0;
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateNormal];
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.viewForRate.frame=originalFrameOfRateView;
        } completion:^(BOOL finished)
         {
             self.viewForDetails.hidden=NO;
         }];
    }
    
}
- (IBAction)OnClickBtnFeedBack:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        RBRatings rating=[self.ratingView getcurrentRatings];
        float rate=rating/2.0;
        if (rating%2 != 0)
        {
            rate += 0.5;
        }
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[UtilityClass sharedObject] showAlertWithTitle:nil andMessage:NSLocalizedString(@"PLEASE_RATE", nil)];
        }
        else
        {
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
            [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:[NSPref GetPreference:PREF_REQUEST_ID] forKey:PARAM_REQUEST_ID];
            [dictParam setObject:[NSString stringWithFormat:@"%.1f",rate] forKey:PARAM_RATING];
            NSString *commt=self.textViewForFeedBackDescription.text;
            if([commt isEqualToString:NSLocalizedString(@"FEEDBACK_NOTE_MSG", nil)])
            {
                [dictParam setObject:@"" forKey:PARAM_COMMENT];
            }
            else
            {
                [dictParam setObject:self.textViewForFeedBackDescription.text forKey:PARAM_COMMENT];
            }
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_FEEDBACK withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"%@",response);
                 if (response)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [NSPref RemovePreference:PREF_REQUEST_ID];
                         [NSPref RemovePreference:PREF_REQUEST_DESCRIPTION];
                         [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
                         [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
                         [NSPref RemovePreference:PREF_VEHICLE_MAKE];
                         [NSPref RemovePreference:PREF_VEHICLE_MODEL];
                         [NSPref RemovePreference:PREF_VEHICLE_YEAR];
                         [NSPref RemovePreference:PREF_REQUEST_ACCEPT];
                         [NSPref RemovePreference:PREF_DOWNPAYMENT];
                         [NSPref RemovePreference:PREF_BID_PRICE];
                         [self.navigationController popToViewController:self.viewObj animated:YES];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                         if([str isEqualToString:@"21"])
                         {
                             [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                         }
                         {
                             [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                         }
                     }
                 }
             }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Helper Methods

-(NSString *)TimeConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter2:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    //  dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

#pragma mark -
#pragma mark - UITextView Delegate Methods

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if([self.textViewForFeedBackDescription.text isEqualToString:NSLocalizedString(@"FEEDBACK_NOTE_MSG", nil)])
    {
        self.textViewForFeedBackDescription.text=@"";
        self.textViewForFeedBackDescription.textColor=[UIColor blackColor];
    }
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if(self.textViewForFeedBackDescription.text.length<1)
    {
        self.textViewForFeedBackDescription.text=NSLocalizedString(@"FEEDBACK_NOTE_MSG", nil);
        self.textViewForFeedBackDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    }
    return YES;
}

@end
