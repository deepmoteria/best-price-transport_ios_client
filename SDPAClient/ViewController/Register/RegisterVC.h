//
//  RegisterVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 24/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"
#import "PTKView.h"
#import "Stripe.h"
#import "PTKTextField.h"

@interface RegisterVC : BaseVC<UIImagePickerControllerDelegate,UINavigationControllerDelegate,PTKViewDelegate,UITextFieldDelegate>
{
    BOOL isProPicAdded;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollObj;

@property (weak, nonatomic) IBOutlet UILabel *lblUploadPicture;
@property (weak, nonatomic) IBOutlet UILabel *lblIAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnGallery;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnTermAndCondition;

- (IBAction)onClickBtnCamera:(id)sender;
- (IBAction)onClickBtnGallery:(id)sender;
- (IBAction)onClickBtnBack:(id)sender;
- (IBAction)onClickBtnRegister:(id)sender;
- (IBAction)onClickBtnCheckBox:(id)sender;
- (IBAction)onClickSubmitOtp:(id)sender;

// for paymentView
@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtCVC;
@property (weak, nonatomic) IBOutlet UITextField *txtOtp;


@property (weak, nonatomic) IBOutlet UIView *viewForAddCard;
@property (weak, nonatomic) IBOutlet UIView *viewForOtp;

@property (weak, nonatomic) IBOutlet UIButton *BtnAddCard;

- (IBAction)onClickBtnAddCard:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblCardinfo;
@property (weak, nonatomic) IBOutlet UILabel *lblOtp;
@property (weak, nonatomic) IBOutlet UILabel *lblOtpMessage;

@end
