//
//  ReferralCodeVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 28/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "ReferralCodeVC.h"

@interface ReferralCodeVC ()

@end

@implementation ReferralCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super setNavBarTitle:NSLocalizedString(@"REFERRAL_CODE", nil)];
    [self SetLocalization];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}
-(void)SetLocalization
{
    self.txtCode.placeholder=NSLocalizedString(@"ENTER_CODE", nil);
    self.textMessage.text=NSLocalizedString(@"REFERRAL_MESSAGE", nil);
    
    [self.btnSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateNormal];
    [self.btnSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateSelected];
    [self.btnAdd setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateNormal];
    [self.btnAdd setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateSelected];
    
    [self.txtCode setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - 
#pragma mark - UIButton Delegate Methods
- (IBAction)onClickBtnSkip:(id)sender
{
    [self performSegueWithIdentifier:REFERRAL_SUCCESS sender:self];
}

- (IBAction)onClickBtnAdd:(id)sender
{
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtCode.text.length<1)
        {
            if(self.txtCode.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"42", nil)];
            }
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
            
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
            [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
            [dictParam setObject:self.txtCode.text forKey:PARAM_CODE];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     if([[response valueForKey:@"success"] boolValue])
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REFERRAL_APPLY", nil)];
                         [self performSegueWithIdentifier:REFERRAL_SUCCESS sender:self];
                     }
                     else
                     {
                         self.txtCode.text=@"";
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UITextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtCode)
    {
        [self.txtCode resignFirstResponder];
        [self.view endEditing:YES];
    }
    return YES;
}

@end
