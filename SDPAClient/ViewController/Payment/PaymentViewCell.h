//
//  PaymentViewCell.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 02/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentViewCell : UITableViewCell
{

}
@property (weak, nonatomic) IBOutlet UIImageView *cellImg;
@property (weak, nonatomic) IBOutlet UILabel *cellCardNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnCardDelete;


@end
