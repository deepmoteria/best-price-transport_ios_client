//
//  AcceptBidDetailVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 11/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "AcceptBidDetailVC.h"
#import "FeedBackVC.h"

@interface AcceptBidDetailVC ()
{
    NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier;
    CGRect originalFrameOfCarrierDetail;
    NSMutableDictionary *carrierData,*biddingData;
    BOOL is_first,ON_PICK_WAY,ON_PICKUP_LOCATION,PICKUP_DONE,ON_DELIVER_WAY,ON_DELIVER_LOCATION,DELIVER_DONE;
    NSTimer *TimerForCheckRequestStatus;
}
@end

@implementation AcceptBidDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForSegueIdentifier=[[NSMutableArray alloc] init];
    carrierData=[[NSMutableDictionary alloc] init];
    originalFrameOfCarrierDetail=self.viewForCarrierDetails.frame;
    self.scrollObjForStatus.contentSize=CGSizeMake(self.view.frame.size.width, 450);
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    [self SetData];
    self.viewForCarrieNote.hidden=YES;
    self.btnCancel.hidden=NO;
    [self checkRequestStatusApp];
    
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_HOME", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_PAYMENT", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_payment",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc] initWithObjects:@"Home",SEGUE_TO_PROFILE,SEGUE_TO_PAYMENT,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"logout", nil];
    
    self.viewObj=nil;
    for (int i=0; i<self.navigationController.viewControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if([vc isKindOfClass:[PickUpVC class]])
        {
            self.viewObj=(PickUpVC *)vc;
        }
    }
    is_first=YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
  //  [UIView animateWithDuration:0.5 animations:^{
        [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS", nil) forState:UIControlStateSelected];
        [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
  //  } completion:^(BOOL finished)
  //   {
         
  //   }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)SetData
{
    // for detailView
    self.btnHideShowDetail.tag=0;
    self.lblVehicleYear.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[NSPref GetPreference:PREF_VEHICLE_MAKE],[NSPref GetPreference:PREF_VEHICLE_MODEL],[NSPref GetPreference:PREF_VEHICLE_YEAR],[NSPref GetPreference:PREF_VEHICLE_TRIM]];
    self.lblPickupAddress.text=[NSPref GetPreference:PREF_SOURCE_ADDRESS];
    self.lblDropoffAddress.text=[NSPref GetPreference:PREF_DESTINATION_ADDRESS];
    
    self.textViewForDescription.text=[NSPref GetPreference:PREF_REQUEST_DESCRIPTION];
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
    }
    self.viewForDetails.hidden=YES;
    [self onClickBtnHideShowDetails:self.btnHideShowDetail];
}
-(void)setCarrierData
{
    // for carrierDetailsView
    [self.img_carrier_profile applyRoundedCornersFull];
    [self.img_carrier_profile downloadFromURL:[carrierData valueForKey:@"picture"] withPlaceholder:nil];
    self.lbl_carrier_name.text=[carrierData valueForKey:@"name"];
    
    NSString *strForPickupTime = [biddingData valueForKey:@"pickup_time"];
    
    NSString *strForPickupDate = [biddingData valueForKey:@"pickup_date"];
    
    NSString *strForDeliveryTime = [biddingData valueForKey:@"delivery_time"];
    
    if(![strForPickupDate isEqualToString:@"0000-00-00"])
    {
        if(![strForPickupTime isEqualToString:@"00:00:00"])
        {
            self.lbl_carrierPickUpTime.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter:[NSString stringWithFormat:@"%@ %@",[biddingData valueForKey:@"pickup_date"],[biddingData valueForKey:@"pickup_time"]]]];
        }
        else
        {
            self.lbl_carrierPickUpTime.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter2:[NSString stringWithFormat:@"%@",[biddingData valueForKey:@"pickup_date"]]]];
        }
    }
    else
    {
        if(![strForPickupTime isEqualToString:@"00:00:00"])
        {
            self.lbl_carrierPickUpTime.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"PICK_UP_TIME", nil),[self TimeConverter1:[NSString stringWithFormat:@"%@",[biddingData valueForKey:@"pickup_time"]]]];
        }
        else
        {
            self.lbl_carrierPickUpTime.text=[NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"PICK_UP_TIME", nil)];
        }
    }
    
    if(![strForDeliveryTime isEqualToString:@"0000-00-00 00:00:00"])
    {
        self.lblDeliveryDate.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"DELIVERY_DATE", nil),[self DateConverter:[biddingData valueForKey:@"delivery_time"]]];
    }
    else
    {
        self.lblDeliveryDate.text=[NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"DELIVERY_DATE", nil)];
    }
    
    self.lbl_carrierMcNumber.text=[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"MC_NUMBER", nil),[carrierData valueForKey:@"mc_license"]];
    
    self.lbl_carrier_rating.text=[carrierData valueForKey:@"rating"];
    self.lbl_carrier_price.text=[NSString stringWithFormat:@"$ %.2f",([[biddingData valueForKey:@"bid_price"] floatValue])];
    self.textViewForNote.textAlignment=NSTextAlignmentCenter;
    self.textViewForNote.text=[biddingData valueForKey:@"description"];
    if(self.textViewForNote.text.length<1)
    {
        self.textViewForNote.text=NSLocalizedString(@"CARRIER_NOT_FIND", nil);
    }
    CGFloat width=self.lbl_carrier_name.intrinsicContentSize.width+5;
    
    if(width<self.lbl_carrier_name.frame.size.width)
    {
        CGRect rateFrame=self.viewForRate.frame;
        rateFrame.origin.x=self.lbl_carrier_name.frame.origin.x+width;
        self.viewForRate.frame=rateFrame;
    }
}

#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    self.lblVehicle.text=NSLocalizedString(@"VEHICLE", nil);
    self.lblPickup.text=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.lblDropoff.text=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lbl_carrierDetailTitle.text=NSLocalizedString(@"CARRIER_DATAIL_TITLE", nil);
    self.lbl_carrier_cash.text=NSLocalizedString(@"CASH", nil);
    self.lblNoteTitle.text=NSLocalizedString(@"CARRIER_NOTE", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    
    self.lblPick1.text=NSLocalizedString(@"CARRIER_ON_PICKUP_LOCATION_WAY",nil);
    self.lblPick2.text=NSLocalizedString(@"CARRIER_RICHED_ON_PICKUP_LOCATION",nil);
    self.lblPick3.text=NSLocalizedString(@"CARRIER_PICKED_VEHICLE",nil);
    self.lblDelivery1.text=NSLocalizedString(@"CARRIER_ON_DELIVER_LOCATION_WAY",nil);
    self.lblDelivery2.text=NSLocalizedString(@"CARRIER_RICHED_ON_DELIVER_LOCATION",nil);
    self.lblDelivery3.text=NSLocalizedString(@"CARRIER_DELIVERY_DONE",nil);
    
    self.imgPick1.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgPick2.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgPick3.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgDelivery1.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgDelivery2.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgDelivery3.image=[UIImage imageNamed:@"status_uncheck"];
    
    [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS",nil) forState:UIControlStateSelected];
    [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateNormal];
    [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateSelected];
    [self.btnCloseOfNote setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateNormal];
    [self.btnCloseOfNote setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateSelected];
    [self.btnConfirm setTitle:NSLocalizedString(@"CONFIRM",nil) forState:UIControlStateNormal];
    [self.btnConfirm setTitle:NSLocalizedString(@"CONFIRM",nil) forState:UIControlStateSelected];
    [self.btnViewNote setTitle:NSLocalizedString(@"VIEW_NOTE", nil) forState:UIControlStateNormal];
    
    self.btnConfirm.enabled=NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:SEGUE_TO_FEEDBACK])
    {
        FeedBackVC *obj=(FeedBackVC *)[segue destinationViewController];
        obj.CarrierDetails=carrierData;
        obj.BiddingData=biddingData;
    
    }
}


#pragma mark -
#pragma mark - UIButton Actions Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
}

- (IBAction)onClickBtnHideShowDetails:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateNormal];
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"VIEW_DETAILS",nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForDetails.hidden=YES;
            self.viewForCarrierDetails.frame=CGRectMake(self.viewForCarrierDetails.frame.origin.x, self.viewForDetails.frame.origin.y, self.viewForCarrierDetails.frame.size.width,self.view.frame.size.height-self.lbl.frame.size.height-self.lbl.frame.origin.y-15);
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        btn.tag=0;
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateNormal];
        [self.btnHideShowDetail setTitle:NSLocalizedString(@"HIDE_DETAILS",nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.viewForCarrierDetails.frame=originalFrameOfCarrierDetail;
        } completion:^(BOOL finished)
         {
             self.viewForDetails.hidden=NO;
         }];
    }
}

- (IBAction)onClickBtnCloseNote:(id)sender
{
    self.viewForCarrieNote.hidden=YES;
}

- (IBAction)onClickBtnCancelRequest:(id)sender
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"CANCEL_REQUEST_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=100;
    [alert show];
}
- (IBAction)onClickBtnViewNote:(id)sender
{
    self.viewForCarrieNote.hidden=NO;
}

- (IBAction)onClickBtnCarrierCall:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]])
    {
        NSString *phoneCallNum = [NSString stringWithFormat:@"tel://+1%@",[carrierData valueForKey:@"phone"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
    }
    else
    {
        [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(@"CALL_ERROR_MESSAGE", nil)];
    }
}

- (IBAction)onClickBtnConfirm:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"CONFORMING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[NSPref GetPreference:PREF_REQUEST_ID] forKey:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_CONFIRM_TRIP withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [TimerForCheckRequestStatus invalidate];
                     TimerForCheckRequestStatus=nil;
                     [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"CONFIRM_DONE_MESSAGE", nil)];
                     [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                 }
             }
             else
             {
                 
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UICollectionView Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"REQUEST_STATUS", nil) forState:UIControlStateSelected];
            [self onClickBtnMenu:self];
            break;
        case 1:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 3:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)LogoutService
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [self RemovePreferences];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [NSPref RemovePreference:PREF_REQUEST_ID];
    [NSPref RemovePreference:PREF_REQUEST_DESCRIPTION];
    [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
    [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
    [NSPref RemovePreference:PREF_VEHICLE_MAKE];
    [NSPref RemovePreference:PREF_VEHICLE_MODEL];
    [NSPref RemovePreference:PREF_VEHICLE_YEAR];
    [NSPref RemovePreference:PREF_REQUEST_ACCEPT];
    [NSPref RemovePreference:PREF_DOWNPAYMENT];
    [NSPref RemovePreference:PREF_BID_PRICE];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self CancelRequest];
                break;
            default:
                break;
        }
    }
    else if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self LogoutService];
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - 
#pragma mark - AppStatus Methods

-(void)checkRequestStatusApp
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",P_GET_REQUEST,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN],PARAM_REQUEST_ID,[NSPref GetPreference:PREF_REQUEST_ID]];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     carrierData=[response valueForKey:@"carrier"];
                     biddingData=[response valueForKey:@"bidding_data"];
                     
                     if([[response valueForKey:@"is_operational"] boolValue])
                         self.lblSetOperational.text = NSLocalizedString(@"RUNNING", nil);
                     else
                         self.lblSetOperational.text = NSLocalizedString(@"NOT_RUNNING", nil);
                     
                     if([[response valueForKey:@"type_transport"] boolValue])
                         self.lblSetTransport.text = NSLocalizedString(@"OPEN", nil);
                     else
                         self.lblSetTransport.text = NSLocalizedString(@"ENCLOSED", nil);
                     
                     [NSPref SetBoolPreference:PREF_IS_RUNNING Value:[[response valueForKey:@"is_operational"] boolValue]];
                     
                     [NSPref SetBoolPreference:PREF_IS_TRANSPORT Value:[[response valueForKey:@"type_transport"] boolValue]];
                     
                     if(is_first)
                     {
                         is_first=NO;
                         [self setCarrierData];
                         if(![[response valueForKey:@"is_confirm"] boolValue])
                         {
                             TimerForCheckRequestStatus=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkRequestStatusApp) userInfo:nil repeats:YES];
                         }
                     }
                     if([[response valueForKey:@"is_confirm"] boolValue])
                     {
                         [TimerForCheckRequestStatus invalidate];
                         TimerForCheckRequestStatus=nil;
                         [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
                     }
                     if([[response valueForKey:@"flag_onpickup_way"] boolValue])
                     {
                         ON_PICK_WAY=YES;
                     }
                     if ([[response valueForKey:@"flag_reached_pickup"] boolValue])
                     {
                         ON_PICKUP_LOCATION=YES;
                         self.btnCancel.hidden=YES;
                     }
                     if ([[response valueForKey:@"flag_pickedup"] boolValue])
                     {
                         PICKUP_DONE=YES;
                     }
                     if ([[response valueForKey:@"flag_ondelivery_way"] boolValue])
                     {
                         ON_DELIVER_WAY=YES;
                     }
                     if ([[response valueForKey:@"flag_reached_delivery"] boolValue])
                     {
                         ON_DELIVER_LOCATION=YES;
                     }
                     if ([[response valueForKey:@"flag_delivery_done"] boolValue])
                     {
                         DELIVER_DONE=YES;
                         [TimerForCheckRequestStatus invalidate];
                         TimerForCheckRequestStatus=nil;
                     }
                     
                    [self SetCarrierStatus];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)SetCarrierStatus
{
    if(ON_PICK_WAY)
    {
        self.imgPick1.image=[UIImage imageNamed:@"status_check"];
        self.lblPick1.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
        if(ON_PICKUP_LOCATION)
        {
            self.lblPick1.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
        }
        else
        {
            self.lblPick1.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
        }
    }
    else
    {
        self.imgPick1.image=[UIImage imageNamed:@"status_uncheck"];
        self.lblPick1.textColor=[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1];
        self.lblPick1.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    }
    if(ON_PICKUP_LOCATION)
    {
        self.imgPick2.image=[UIImage imageNamed:@"status_check"];
        self.lblPick2.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
        if(PICKUP_DONE)
        {
            self.lblPick2.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
        }
        else
        {
            self.lblPick2.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
        }
    }
    else
    {
        self.imgPick2.image=[UIImage imageNamed:@"status_uncheck"];
        self.lblPick2.textColor=[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1];
        self.lblPick2.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    }
    if(PICKUP_DONE)
    {
        self.imgPick3.image=[UIImage imageNamed:@"status_check"];
        self.lblPick3.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
        if(ON_DELIVER_WAY)
        {
            self.lblPick3.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
        }
        else
        {
            self.lblPick3.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
        }
    }
    else
    {
        self.imgPick3.image=[UIImage imageNamed:@"status_uncheck"];
        self.lblPick3.textColor=[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1];
        self.lblPick3.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    }
    if(ON_DELIVER_WAY)
    {
        self.imgDelivery1.image=[UIImage imageNamed:@"status_check"];
        self.lblDelivery1.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
        if(ON_DELIVER_LOCATION)
        {
            self.lblDelivery1.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
        }
        else
        {
            self.lblDelivery1.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
        }
    }
    else
    {
        self.imgDelivery1.image=[UIImage imageNamed:@"status_uncheck"];
        self.lblDelivery1.textColor=[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1];
        self.lblDelivery1.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    }
    if(ON_DELIVER_LOCATION)
    {
        self.imgDelivery2.image=[UIImage imageNamed:@"status_check"];
        self.lblDelivery2.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
        if(DELIVER_DONE)
        {
            self.lblDelivery2.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
        }
        else
        {
            self.lblDelivery2.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
        }
    }
    else
    {
        self.imgDelivery2.image=[UIImage imageNamed:@"status_uncheck"];
        self.lblDelivery2.textColor=[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1];
        self.lblDelivery2.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    }
    if(DELIVER_DONE)
    {
        self.imgDelivery3.image=[UIImage imageNamed:@"status_check"];
        self.lblDelivery3.textColor=[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
        self.lblDelivery3.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
        self.btnConfirm.enabled=YES;
    }
    else
    {
        self.imgDelivery3.image=[UIImage imageNamed:@"status_uncheck"];
        self.lblDelivery3.textColor=[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1];
        self.lblDelivery3.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
        self.btnConfirm.enabled=NO;
    }
}
-(void)CancelRequest
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"CANCEL_REQUESTING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        [dictParam setObject:[NSPref GetPreference:PREF_REQUEST_ID] forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"CANCEL_REQUEST_SUCCESS", nil)];
                     [NSPref RemovePreference:PREF_REQUEST_ID];
                     [NSPref RemovePreference:PREF_REQUEST_DESCRIPTION];
                     [NSPref RemovePreference:PREF_SOURCE_ADDRESS];
                     [NSPref RemovePreference:PREF_DESTINATION_ADDRESS];
                     [NSPref RemovePreference:PREF_VEHICLE_MAKE];
                     [NSPref RemovePreference:PREF_VEHICLE_MODEL];
                     [NSPref RemovePreference:PREF_VEHICLE_YEAR];
                     [NSPref RemovePreference:PREF_REQUEST_ACCEPT];
                     [NSPref RemovePreference:PREF_DOWNPAYMENT];
                     [NSPref RemovePreference:PREF_BID_PRICE];

                     [self.navigationController popToViewController:self.viewObj animated:YES];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Helper Methods

-(NSString *)TimeConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate:date];
    
    return dateStr;
}

-(NSString *)TimeConverter2:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    //  dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)ReverseDateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)ReverseDateConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

@end
