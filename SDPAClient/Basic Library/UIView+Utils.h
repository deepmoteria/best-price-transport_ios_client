//
//  UIView+Utils.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)
- (void)applyRoundedCorners;
- (void)applyRoundedCornersLess;
- (void)applyRoundedCornersFull;
- (void)applyRoundedCornersFullWithColor:(UIColor *)color;
@end
